import math
import json
import requests
import time

theip = 'http://192.168.10.254/gps'
gpslog = 'gps_log_Fic39xA4lk049TAWgTAb.csv'
# timestamp,utcTime,lat,lon,altMsl,velN,velE,velD,UpTime,pAcc,pDop,satsUsed,satsTracked,fixFlags,fixType,velocity
with open(gpslog, 'r') as f:
    logs = []
    for l in f:
        logs.append(l)
# Documentation for parameters: http://micasense.github.io/rededge-api/api/http.html#gps 

# start later in the GPS logs to where it is actually in the air 
# change to logs[1:] with gps_log_Fic39xA4lk049TAWgTAb.csv 
# to slowly watch it go up to altitude. 
for log in logs[1:]:
    # split line by comma into list 
    log = log.split(',')
    # the format from my log output is a bit different 
    # than the camera expects, so change accordingly 
    utc_time = log[0].replace(' ', 'T') + 'Z'
    latitude = float(log[2]) * math.pi/180
    longitude = float(log[3]) * math.pi/180
    altitude = float(log[4])
    vel_n = log[5]
    vel_e = log[6]
    vel_d = log[7]
    p_acc = log[9]
    fix3d = True
    message = {"latitude": latitude, "longitude": longitude, "altitude": altitude, "vel_n": vel_n, \
              "vel_e": vel_e, "vel_d": vel_d, "fix3d": fix3d, "utc_time": utc_time}
    print("Desired injected data: ")
    print(message)
    # be sure to use json = instead of data=, which is usual for requests.post
    # it seems requests tries to correct the string 
    # and gives a bad utc_time error with the API
    r = requests.post(url = theip, json = message)
    print("Actual response data: ")
    print(r.json())
    time.sleep(0.5)
    print("-----")